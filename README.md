# SoftAtHome Component Manifest
1. [Introduction](#Introduction)
2. [How-To setup build environment](#how-to-setup-build-environment)
3. [How-To setup opkg package repository](#how-to-setup-opkg-package-repository)
4. [How-To setup local deb package repository](#how-to-setup-local-deb-package-repository)
5. [How-To use gmap](#how-to-use-gmap)

## 1. Introduction
This document describes how to build, package, host and use the SoftAtHome Process Communication Bus (`PCB`), NetworkModel (`NeMo`) and Topology manager (`gMap`).
The compilation and packaging is handled by Yocto.
We currently support building for `x86-64` and the `arm cortex a15-neon-vfpv4`.
When building for `x86`, the target packages are `.deb`.
The resulting packages are `.ipk` for the arm board.
The `.ipk` packages are compatible with `opkg` on a supported board (e.g. `Netgear Nighthawk X4S`).
Instructions on how to host these packages (for demo purposes) is also provided.
Finally, the last section details how to run these services and interact with them using our client `pcb_cli`.

## 2. How-To setup build environment
Follow these instructions to get a Yocto build environment setup.
The guide was written with a debian based system in mind (e.g. with the use of `apt`).
The minimum requirements are Debian 10 or Ubuntu 18.04 to have access to `python3.6+` (you can also achieve this by activating experimental or backport repositories on older Debian/Ubuntu systems).

Install the following host tools
```bash
sudo apt-get update && sudo apt-get install \
        curl gawk wget git-core subversion diffstat \
        unzip sysstat texinfo gcc-multilib build-essential \
        chrpath socat python python3 cpio xz-utils locales \
        screen tmux sudo fluxbox procps tightvncserver \
        openssh-client openssh-server
```

Install the Google repo tool for using this manifest.
```bash
mkdir ~/.local/bin
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/.local/bin/repo
chmod a+x ~/.local/bin
```
> _NOTE:_ `~/.local/bin` should be on your `$PATH`, if not add it (optionally add this to your `~/.bashrc`) with `PATH="${PATH}:${HOME}/.local/bin"`.

Download the Yocto buildsystem and all required Yocto and SoftAtHome layers.
```bash
export WORKSPACEDIR=<path-of-your-workspace>
mkdir -p ${WORKSPACEDIR}
cd ${WORKSPACEDIR}

repo init -u ssh://git@gitlab.com/soft.at.home/buildsystems/yocto/manifests
repo sync
```

Setup the Yocto build environment, for this step we have multiple options depending on the target configuration (`x86` or `arm`).
* Set the Yocto build environment to match `x86-64`.
```bash
export TEMPLATECONF=meta-sah/tools/meta-sah-prpl/conf/genericx86-64/
source oe-init-build-env
```
* Set the Yocto build environment to match the supported target board `Netgear Nighthawk X4S`.
```bash
export TEMPLATECONF=meta-sah/tools/meta-sah-prpl/conf/cortexa15-neon-vfpv4/
source oe-init-build-env
```

You can now build the provided packages.
To build the main packagegroup run the following command.
```bash
bitbake packagegroup-core-gmap
```

* You can find the `.deb` packages for `x86` in the following directory: `${WORKSPACEDIR}/build/tmp-musl/deploy/deb/core2-64/`.
* You can find the `.ipk` packages for `arm` in the following directory: `${WORKSPACEDIR}/build/tmp-musl/deploy/ipk/cortexa15-neon-vfpv4/`.

## 3. How-To setup opkg package repository
The following guide will result in an easy to setup (unsigned) opkg package repository.
This repository is well suited for demos, but should not be used in a production environment.

You will need to install at least the following packages:
```bash
sudo apt update && sudo apt install make perl python3 wget
```

Get the correct version of the opkg-utils toolset from Yocto
```bash
export OPKG_VERSION="0.4.2"
wget https://git.yoctoproject.org/cgit/cgit.cgi/opkg-utils/snapshot/opkg-utils-${OPKG_VERSION}.tar.gz && tar -xzf opkg-utils-${OPKG_VERSION}.tar.gz
cd opkg-utils-${OPKG_VERSION}
make
sudo make install
```

Copy all the desired `.ipk` files from the deploy directory (`${WORKSPACEDIR}/build/tmp-musl/deploy/ipk/all/` and `${WORKSPACEDIR}/build/tmp-musl/deploy/ipk/cortexa15-neon-vfpv4/`) to you package repository directory and generate its index.
```bash
cd <path-to-opkg-repository>
opkg-make-index . > Packages && gzip -c Packages > Packages.gz
```

Finally, host the package repository by enabling a simply http server for the opkg repository.
```bash
cd <path-to-opkg-repository>
python3 -m http.server 8080
```

You can now reach your package repository on your IP address (on port `8080`).

## 4. How-To setup local deb package repository
The following guide will result in the ability to install the build debian packages on your local machine (or on a local container).
Similar to the repository from the previous section, it is only suited for demo purposes.

Copy all the desired `.deb` files from the deploy directory (`${WORKSPACEDIR}/build/tmp-musl/deploy/deb/all/` and `${WORKSPACEDIR}/build/tmp-musl/deploy/deb/core2-64/`) to you package repository directory.
```bash
mkdir -p <path-to-deb-packages>
cp ${WORKSPACEDIR}/build/tmp-musl/deploy/deb/all/* ${WORKSPACEDIR}/build/tmp-musl/deploy/deb/core2-64/* <path-to-deb-packages>
```

To avoid clashing with your local system, running the following commands in a container is suggested.
To setup the demo environment we use docker containers.
To install docker on your local system, please refer to the official docker documentation ([ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/), [debian](https://docs.docker.com/install/linux/docker-ce/debian/)).
We start the _gateway_ container as privileged because we need to be able to create a bridge interface.
```bash
sudo docker network create --driver bridge gmap-demo
sudo docker run -dit --privileged --name gmap-demo -v <path-to-deb-packages>:<path-to-deb-packages> ubuntu bash
sudo docker run -dit --name demo-device --network gmap-demo ubuntu bash
sudo docker network connect gmap-demo gmap-demo
sudo docker attach gmap-demo
```

You will need to install at least the following packages:
```bash
apt update && apt install -y dpkg-dev iproute2 inetutils-syslogd bridge-utils inetutils-ping
```

Navigate to the directory holding your `.deb` packages and generate the index.
```bash
cd <path-to-deb-packages>
dpkg-scanpackages . /dev/null | gzip -9c > Packages.gz
```

Now we setup the interfaces of our container to match what is expected by the shipped `odl` configuration of `NeMo`.
```bash
brctl addbr br-lan
brctl addif br-lan eth1
ip_address=$(ip address show dev eth1 scope global | awk '/inet / {split($2,var,"/"); print var[1]}')
ip address del ${ip_address}/16 dev eth1
ip address add ${ip_address}/16 dev br-lan
ip link set dev br-lan up
```

Finally add the local directory to your sources list and update.
We also set the local package repository to have priority over remote repositories so we always pick the locally created packages.
The dpkg option to force overwrite is required to allow the musl package to overwrite any glibc installed files.
```bash
cat <<EOF > /etc/apt/preferences
Package: *
Pin: origin ""
Pin-Priority: 1001
EOF

echo "deb [trusted=yes] file:<path-to-deb-packages> ./" >> /etc/apt/sources.list
apt update
apt install -o Dpkg::Options::="--force-overwrite" packagegroup-core-gmap libmnl
```

> _NOTE:_ The following commands are required to prevent issues and errors on the `x86` system.
They are also mentioned in the next section, but in case you read over them, they are summarized here as well.
```bash
ln -s -f bash /bin/sh
/etc/init.d/inetutils-syslogd start
```

## 5. How-To use gmap
This section will detail how to install, run and test the SoftAtHome opensource topology tool gMap on the supported arm board.
First enable your custom hosted package feed to a connected running target (currently the packages built are supported by the following target: `Netgear Nighthawk X4S`).
```bash
echo "src/gz sah_feed http://<package-repository-ip>:8080" >> /etc/opkg/customfeeds.conf
```

As the hosted package repository doesn't support signed packages, you must disable this check by modifying the default opkg configuration file `/etc/opkg.conf` and remove the option `option check_signature`.
You can now interact with the package repository setup in [step 3](#how-to-setup-package-repository).
```bash
opkg update
```

Install the previously built packagegroup
```bash
opkg install packagegroup-core-gmap
```

> _NOTE:_ As there is still a mismatch in the naming of the musl library in Yocto and in openWrt, an additional symlink should be made on the target `ln -s libc.so /lib/ld-musl-arm.so.1` to allow the applications to start up correctly.

> _NOTE:_ when running the `x86` packages, the init scripts are not designed for use outside an embedded system.
You can prevent some errors by symlinking `/bin/sh` to `/bin/bash` (i.e. `ln -s -f bash /bin/sh`).
To avoid some other errors, please run a syslog instance with the command `/etc/init.d/inetutils-syslogd start`.
Be sure to have followed the steps in the previous section to have all required packages and containers setup.

This package results in all the needed services and libraries to be installed on your target.
Once installed you can start the different services manually, or reboot the device for them to start up on their own.
When starting manually, allow some time between starting the different services to make sure that they are correctly running.
```bash
/etc/init.d/sysbus start
/etc/init.d/nemo-core start
/etc/init.d/netdev start
/etc/init.d/nemo-clients start
/etc/init.d/gmap start
```

### PCB
With the `pcb` bus running, you can interact with it at run-time with the `pcb_cli` tool.
PCB works with a data model made of objects, parameters and RPCs (remote procedure call): the data model is the API set exposed by a plugin.
All items are named.
Plugins will expose a datamodel of their own, and clients will require parts of this datamodel via the bus.
Clients can also require execution of a RPC in the remote plugin, with arguments and results forwarded to the client.

Here is an example of data model:
```
Object
Object.InnerObject1
Object.InnerObject1.Param1=Value1
Object.InnerObject1.Param2=Value2
Object.InnerObject2.instance1
Object.InnerObject2.instance2
```

You can list an object by using the `?` operator.
The `?` operator can be used on any object or parameter.
```
pcb_cli "NeMo?"
```
> _NOTE:_ `pcb_cli` can also be used interactively, simply run `pcb_cli` to get dropped in the _cli_. The following snippets will assume you are running in this context.

The `&` operator can be used to put a subscription on a datamodel object or parameter.
```
Devices.Device?&
```

### NeMo
NeMo stands for Network Model.
The NeMo data model represents the network stack configuration, and the NeMo modules act as a backend, between the representation and the drivers/applications.
NeMo clients will then be able to browse the NeMo tree using the API.

You can for example list all information gathered on the bridge interface.
```
NeMo.Intf.br-lan?
```

### gMap
gMap is the topology module of the gateway.
It maps the whole local network topology, and provides access to other plugins, to drive the devices (enable, ...).
To view information on your connected device (e.g. laptop) run the following `pcb_cli` command.
```
Devices.Device.<your-mac-address>?
```

The module determines connected devices based on the arp information gathered by `NetDev`.
If you don't see your device listed, try flushing the arp cache and sending a new ping to your/from your device (e.g. following the `x86` tutorial: `ip neigh flush all && ping -c1 demo-device`).
